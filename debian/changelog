viewnior (1.8-5) unstable; urgency=medium

  * Set release to unstable.
  * Remove trailing whitespaces in debian/changelog.
  * Bump copyright year in debian/copyright.
  * Bump Standards-Version to use 4.7.0. No changes were required.

 -- Dominik Szmek <minikdo@riseup.net>  Wed, 19 Jun 2024 20:45:46 +0200

viewnior (1.8-4) experimental; urgency=medium

  * Bump dependency on exiv2 to use 0.28.
    thanks to Pino Toscano <pino@debian.org>.
    Closes: #1071028

 -- Dominik Szmek <minikdo@riseup.net>  Wed, 12 Jun 2024 22:24:42 +0200

viewnior (1.8-3) unstable; urgency=medium

  * Fix build by removing override_dh_auto_install.

 -- Dominik Szmek <minikdo@riseup.net>  Thu, 14 Sep 2023 17:22:55 +0530

viewnior (1.8-2) unstable; urgency=medium

  * New maintainer. Closes: #1049351
  * Update package to use debhelper-compat 13
  * Fix wrong viewnior.svg file permissions.
  * Remove unnecessary dh argument.
  * Fix inconsistent appstream metadata license.
  * Add hardening flags.
  * Bump Standards-Version to use 4.6.2:
    - Document Rules-Requires-Root as no
    - Update copyright file to use https in Format field
  * Update copyright Source file.

 -- Dominik Szmek <minikdo@riseup.net>  Tue, 12 Sep 2023 12:46:42 +0530

viewnior (1.8-1) unstable; urgency=medium

  * QA upload
  * Orphan the package (see: #1049351)
  * Replace Homepage (Closes: #953310)
  * d/watch: Scan GitHub tags
  * New upstream version 1.8 (Closes: #943837)
  * Build with meson
  * Build with gtk3 (Closes: #967796)

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

 -- Bastian Germann <bage@debian.org>  Tue, 29 Aug 2023 19:54:35 +0200

viewnior (1.6-1) unstable; urgency=medium

  * Imported Upstream version 1.6 (Closes: #785542)

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Wed, 23 Dec 2015 15:01:57 +0100

viewnior (1.5-2) unstable; urgency=medium

  * Added --parallel build (Closes: #802277)
  * Menu file removed

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Thu, 29 Oct 2015 09:14:02 +0100

viewnior (1.5-1) unstable; urgency=medium

  * Imported Upstream version 1.5
  * Bump standards to 3.9.6, no changes needed
  * Fix package-contains-timestamped-gzip by adding -9n to gzip in d/rules
  * d/copyright: removed file ltmain.sh which is no longer present
  * Patch add-keyswords-desktop.patch removed, changes merged upstream
  * Added d/gbp.conf

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Tue, 12 May 2015 15:54:37 +0200

viewnior (1.4-2) unstable; urgency=medium

  [ Laszlo Kajan ]
  * rm old watch line
  * ignoring generated files

  [ Dariusz Dwornikowski ]
  * Copyright fixed, removed changelog from docs, changelog handling in rules
  * wrap-and-sorted debian/control, debian/copyright
  * Deleted ignored files
  * Dropped build-depends on libexiv2-12, thanks Colin Watson (Closes: #756489)
  * Fixed debian/watch file
  * Patch added to add _Keywords to desktop file

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Sat, 02 Aug 2014 12:23:55 +0200

viewnior (1.4-1) unstable; urgency=medium

  * Initial release (Closes: #582090)

 -- Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>  Fri, 07 Feb 2014 13:03:56 +0100
